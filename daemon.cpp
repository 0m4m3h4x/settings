#include "daemon.h"
#include <QDebug>

#include <QDBusConnection>

Daemon::Daemon()
{
    qDebug() << "Starting in daemon mode.";

    m_background = new Background(this);
    new BackgroundAdaptor(m_background);

    m_theme = new Theme(this);
    new ThemeAdaptor(m_theme);

    m_daemonSettings = new DaemonSettings(this);
    new DaemonSettingsAdaptor(m_daemonSettings);

    QDBusConnection connection = QDBusConnection::sessionBus();
    connection.registerObject("/Background", m_background);
    connection.registerObject("/Theme", m_theme);
    connection.registerObject("/DaemonSettings", m_daemonSettings);
    connection.registerService("Miko.Settings");

    this->connect(m_daemonSettings, &DaemonSettings::doReload, this, &Daemon::handleReload);

    qDebug() << "D-Bus ready.";
}

void Daemon::handleReload() {
    qDebug() << "Reloading settings.";
    m_theme->reload();
}
