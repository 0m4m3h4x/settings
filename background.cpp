#include "background.h"
#include <QDir>
#include <QDirIterator>

Background::Background(QObject *parent)
    : QObject{parent},
      m_settings("Miko", "Settings")
{

}


QStringList Background::backgrounds() {
    QStringList list;
    QDirIterator backgrounds("/usr/share/backgrounds", {"*.png", "*.jpg", "*.svg", "*.webp"}, QDir::Files, QDirIterator::Subdirectories);
    while (backgrounds.hasNext()) {
        list << backgrounds.next();
    }
    return list;
}

QString Background::background() {
    return m_settings.value("appearance/background", "/usr/share/backgrounds/omamehax/omamehax-l.png").toString();
}

void Background::setBackground(QString background) {
    m_settings.setValue("appearance/background", background);
    emit this->backgroundChanged();
}
