import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import Miko.Asuki 1.0
import Miko.Settings 1.0

Window {
    id: root
    width: 800
    height: 500
    visible: true
    title: view.currentItem.title || qsTr("Settings")

    Globals {
        id: globals
    }

    StackView {
        id: view
        anchors.fill: parent
        clip: true

        initialItem: Index {}
    }
}
