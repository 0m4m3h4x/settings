#ifndef DAEMONSETTINGS_H
#define DAEMONSETTINGS_H

#include <QObject>

class DaemonSettings : public QObject
{
    Q_OBJECT
public:
    explicit DaemonSettings(QObject *parent = nullptr);

public slots:
    void reload();

signals:
    void doReload();
};

#endif // DAEMONSETTINGS_H
