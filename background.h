#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <QObject>
#include <QQmlEngine>
#include "settings_adaptor.h"

class Background : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList backgrounds READ backgrounds CONSTANT)
    Q_PROPERTY(QString background READ background WRITE setBackground NOTIFY backgroundChanged)
    QML_ELEMENT

public:
    explicit Background(QObject *parent = nullptr);

    QStringList backgrounds();

public slots:
    QString background();
    void setBackground(QString background);

signals:
    void backgroundChanged();

private:
    QSettings m_settings;
};

#endif // BACKGROUND_H
