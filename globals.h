#ifndef GLOBALS_H
#define GLOBALS_H

#include <QObject>
#include "theme.h"

class Globals : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Theme *theme READ theme CONSTANT)
    QML_ELEMENT

public:
    explicit Globals(QObject *parent = nullptr);
    Theme *theme();
};

#endif // GLOBALS_H
