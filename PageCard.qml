import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15
import Miko.Asuki 1.0
import Miko.Settings 1.0

Rectangle {
    id: root
    width: layout.implicitWidth + 24 * 2
    height: layout.implicitHeight + 24 * 2
    color: TailwindColor.slate[globals.theme.darkMode ? 700 : 50]
    radius: 16
    border.width: 1
    border.color: TailwindColor.slate[globals.theme.darkMode ? 900 : 300]

    required property string title
    required property string description

    Globals {
        id: globals
    }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 24
        spacing: 8

        Label {
            text: root.title
            font.weight: Font.DemiBold
        }

        Label {
            text: root.description
            color: TailwindColor.slate[globals.theme.darkMode ? 300 : 700]
        }
    }

    layer.enabled: true
    layer.effect: DropShadow {
        verticalOffset: 1
        radius: 2
        samples: radius * 2 + 1
        color: TailwindColor.opacity(TailwindColor.slate[globals.theme.darkMode ? 900 : 300], 0.4)
    }
}
