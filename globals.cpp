#include "globals.h"

Globals::Globals(QObject *parent)
    : QObject{parent}
{

}

Theme *g_theme = nullptr;

Theme *Globals::theme() {
    if (g_theme == nullptr) {
        qDebug() << "Globals: Instantiating theme...";
        g_theme = new Theme();
    }

    return g_theme;
}
