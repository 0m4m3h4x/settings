import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Miko.Asuki 1.0
import Miko.Settings 1.0

Rectangle {
    anchors.fill: parent
    color: TailwindColor.slate[globals.theme.darkMode ? 800 : 200]

    Globals {
        id: globals
    }

    Flickable {
        anchors.fill: parent
        anchors.margins: 24
        contentHeight: layout.implicitHeight

        ScrollBar.vertical: ScrollBar {}

        ColumnLayout {
            id: layout
            anchors.fill: parent
            spacing: 24

            RowLayout {
                spacing: 8

                IconLabel {
                    text: "settings"
                    font.pixelSize: 32
                }

                Label {
                    text: qsTr("Settings")
                    font.pixelSize: 24
                    font.weight: Font.DemiBold
                }
            }

            Flow {
                Layout.fillWidth: true
                spacing: 24

                PageCard {
                    title: qsTr("Appearance")
                    description: qsTr("Change the appearance of your computer.")
                }
                PageCard {
                    title: qsTr("Network")
                    description: qsTr("Get your computer connected to the internet.")
                }
                PageCard {
                    title: qsTr("Users")
                    description: qsTr("Add, remove or modify users on your system.")
                }
                PageCard {
                    title: qsTr("System")
                    description: qsTr("Update your system and view technical information.")
                }
            }

            Item {
                Layout.fillHeight: true
            }
        }
    }
}
