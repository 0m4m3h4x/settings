#ifndef THEME_H
#define THEME_H

#include <QObject>
#include <QQmlEngine>
#include "settings_adaptor.h"

class Theme : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString accentColor READ accentColor WRITE setAccentColor NOTIFY accentColorChanged)
    Q_PROPERTY(bool darkMode READ darkMode WRITE setDarkMode NOTIFY darkModeChanged)
    QML_ELEMENT

public:
    explicit Theme(QObject *parent = nullptr);

    void reload();

public slots:
    QString accentColor();
    bool darkMode();

    void setAccentColor(QString accentColor);
    void setDarkMode(bool darkMode);

signals:
    void accentColorChanged();
    void darkModeChanged();

private:
    QSettings m_settings;
};

#endif // THEME_H
