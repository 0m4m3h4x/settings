QT += dbus quick quickcontrols2

HEADERS += \
    background.h \
    daemon.h \
    daemonsettings.h \
    globals.h \
    theme.h

SOURCES += \
        background.cpp \
        daemon.cpp \
        daemonsettings.cpp \
        globals.cpp \
        main.cpp \
        theme.cpp

TRANSLATIONS += \
    translations/settings_en_US.ts

RESOURCES += \
    settings.qrc

DBUS_ADAPTORS += settings.xml

CONFIG += lrelease embed_translations

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

TARGET = miko-settings

unix: target.path = /usr/bin
!isEmpty(target.path): INSTALLS += target
