#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QLocale>
#include <QTranslator>

#include <QCommandLineParser>
#include <QCommandLineOption>

#include "background.h"
#include "theme.h"
#include "globals.h"

#include "daemon.h"

#include <QQuickStyle>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setApplicationName("settings");
    app.setApplicationDisplayName(QGuiApplication::tr("Settings"));
    app.setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription(QGuiApplication::tr("Settings app and daemon for Miko."));
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption daemonOption("daemon", QGuiApplication::tr("Run in daemon mode."));
    parser.addOption(daemonOption);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "settings_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    parser.process(app);

    if (parser.isSet(daemonOption)) {
        Daemon daemon;
        return app.exec();
    }

    qmlRegisterType<Background>("Miko.Settings", 1, 0, "Background");
    qmlRegisterType<Theme>("Miko.Settings", 1, 0, "Theme");
    qmlRegisterType<Globals>("Miko.Settings", 1, 0, "Globals");

    QQuickStyle::setStyle("Miko.Asuki");

    QQmlApplicationEngine engine;
    const QUrl url("qrc:/main.qml");
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
