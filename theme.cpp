#include "theme.h"
#include <QDir>
#include <QDirIterator>

Theme::Theme(QObject *parent)
    : QObject{parent},
      m_settings("Miko", "Settings")
{

}

QString Theme::accentColor() {
    return m_settings.value("appearance/accentColor", "blue").toString();
}

void Theme::setAccentColor(QString accentColor) {
    m_settings.setValue("appearance/accentColor", accentColor);
    emit this->accentColorChanged();
}

bool Theme::darkMode() {
    return m_settings.value("appearance/darkMode", false).toBool();
}

void Theme::setDarkMode(bool darkMode) {
    m_settings.setValue("appearance/darkMode", darkMode);
    emit this->darkModeChanged();
}

void Theme::reload() {
    m_settings.sync();
    emit this->darkModeChanged();
    emit this->accentColorChanged();
}
