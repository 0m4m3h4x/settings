#ifndef DAEMON_H
#define DAEMON_H
#include "background.h"
#include "theme.h"
#include "daemonsettings.h"
#include "settings_adaptor.h"
#include <QObject>

class Daemon : public QObject
{
    Q_OBJECT
public:
    Daemon();

    void handleReload();

private:
    Background *m_background;
    Theme *m_theme;
    DaemonSettings *m_daemonSettings;
};

#endif // DAEMON_H
